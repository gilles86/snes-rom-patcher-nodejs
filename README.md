# What does this tool do?
It uses nodejs to Autopatch a SNES Rom with all Patches given and saves them as new Games.
This tool was used by me to Patch Mario Rom Hacks.
It searches for bps/ips or zip files.
The Zip Files will be extracted just with the bps/ips files and the zips will be deleted afterwards (can be switched off)

# How to use
- Install NodeJS (https://nodejs.org/en/download/)
- Open your Terminal in the same Folder as this README
- run "npm install"
- Open the file "src/Patcher.js" with a text-editor and edit the Config Part
- in Terminal run from the (Root) Folder (where the README is) "npm run start"
- You will see a log. After it finished you should have new Roms. Have fun :)

# Agreement
This tool was written for my personal use. I do not guarantee anything.
If you have questions join my Discord: https://discord.gg/BZxbduJtnD
You can also find me in most german SMW Kaizo Discords (Saphros/Gilles/Gamedev Tutorial).
